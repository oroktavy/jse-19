package ru.aushakov.tm.util;

import ru.aushakov.tm.exception.general.CanNotEncryptPasswordException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public interface HashUtil {

    String SECRET = "12345MYSECRET12345";

    Integer ITERATION_NUM = 12345;

    static String salt(final String value) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < ITERATION_NUM; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    static String md5(final String value) {
        if (value == null) return null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] byteArray = md.digest(value.getBytes(StandardCharsets.UTF_8));
            final StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < byteArray.length; i++) {
                buffer.append(Integer.toHexString((byteArray[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return buffer.toString();
        } catch (Exception e) {
            throw new CanNotEncryptPasswordException(e);
        }
    }

}
