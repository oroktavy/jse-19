package ru.aushakov.tm.exception.empty;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Provided id is empty!");
    }

    public EmptyIdException(final String entityName) {
        super("Provided " + entityName + " id is empty!");
    }

}
