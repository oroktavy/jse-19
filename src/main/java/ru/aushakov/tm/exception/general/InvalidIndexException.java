package ru.aushakov.tm.exception.general;

public class InvalidIndexException extends RuntimeException {

    public InvalidIndexException() {
        super("Index is invalid!");
    }

    public InvalidIndexException(final Integer index) {
        super("The value '" + index + "' entered is not a valid index!");
    }

    public InvalidIndexException(final String textIndex) {
        super("The value '" + textIndex + "' entered is not a number!");
    }

}
