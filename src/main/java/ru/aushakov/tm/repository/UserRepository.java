package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        for (final User user : list) {
            if (login.equalsIgnoreCase(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeOneByLogin(final String login) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public User setPassword(
            final String login,
            final String newPassword
    ) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(newPassword));
        return user;
    }

    @Override
    public User updateOneById(
            final String id,
            final String lastName,
            final String firstName,
            final String middleName
    ) {
        final User user = findOneById(id);
        if (user == null) return null;
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User updateOneByLogin(
            final String login,
            final String lastName,
            final String firstName,
            final String middleName
    ) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        return user;
    }

}
