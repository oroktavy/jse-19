package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.IBusinessRepository;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    public AbstractBusinessRepository() {
    }

    @Override
    public E findOneByName(final String name) {
        for (final E entity : list) {
            if (name.equals(entity.getName())) return entity;
        }
        return null;
    }

    @Override
    public E removeOneByName(final String name) {
        final E entity = findOneByName(name);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E updateOneById(
            final String id,
            final String name,
            final String description
    ) {
        final E entity = findOneById(id);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateOneByIndex(
            final Integer index,
            final String name,
            final String description
    ) {
        final E entity = findOneByIndex(index);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E startOneById(final String id) {
        final E entity = findOneById(id);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E startOneByIndex(final Integer index) {
        final E entity = findOneByIndex(index);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E startOneByName(final String name) {
        final E entity = findOneByName(name);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E finishOneById(final String id) {
        final E entity = findOneById(id);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public E finishOneByIndex(final Integer index) {
        final E entity = findOneByIndex(index);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public E finishOneByName(final String name) {
        final E entity = findOneByName(name);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public E changeOneStatusById(final String id, final Status status) {
        final E entity = findOneById(id);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeOneStatusByIndex(final Integer index, final Status status) {
        final E entity = findOneByIndex(index);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeOneStatusByName(final String name, final Status status) {
        final E entity = findOneByName(name);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

}
