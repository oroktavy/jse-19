package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public Task assignTaskToProject(final String taskId, final String projectId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId())) {
                projectTasks.add(task);
            }
        }
        return projectTasks;
    }

    @Override
    public List<Task> removeAllTasksByProjectId(final String projectId) {
        final List<Task> deletedTasks = new ArrayList<>();
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId())) {
                deletedTasks.add(task);
            }
        }
        list.removeAll(deletedTasks);
        return deletedTasks;
    }

}
