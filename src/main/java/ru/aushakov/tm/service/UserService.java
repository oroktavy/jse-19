package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.api.service.IUserService;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.empty.EmptyLoginException;
import ru.aushakov.tm.exception.empty.EmptyPasswordException;
import ru.aushakov.tm.exception.general.InvalidRoleException;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.HashUtil;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.repository = userRepository;
        this.userRepository = userRepository;
    }

    @Override
    public User findOneByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findOneByLogin(login);
    }

    @Override
    public User removeOneByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.removeOneByLogin(login);
    }

    @Override
    public User add(
            final String login,
            final String password,
            final String email
    ) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(password)) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        if (!StringUtils.isEmpty(email)) user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @Override
    public void add(
            final String login,
            final String password,
            final String email,
            final String roleId
    ) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(password)) throw new EmptyPasswordException();
        final Role role = Role.toRole(roleId);
        if (role == null) throw new InvalidRoleException(roleId);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        if (!StringUtils.isEmpty(email)) user.setEmail(email);
        user.setRole(role);
        userRepository.add(user);
    }

    @Override
    public User setPassword(
            final String login,
            final String newPassword
    ) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(newPassword)) throw new EmptyPasswordException();
        return userRepository.setPassword(login, newPassword);
    }

    @Override
    public User updateOneById(
            final String id,
            final String lastName,
            final String firstName,
            final String middleName
    ) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return userRepository.updateOneById(id, lastName, firstName, middleName);
    }

    @Override
    public User updateOneByLogin(
            final String login,
            final String lastName,
            final String firstName,
            final String middleName
    ) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.updateOneByLogin(login, lastName, firstName, middleName);
    }

}
