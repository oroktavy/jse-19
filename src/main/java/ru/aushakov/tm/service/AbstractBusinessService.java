package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.IBusinessRepository;
import ru.aushakov.tm.api.service.IBusinessService;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.empty.EmptyDescriptionException;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.empty.EmptyNameException;
import ru.aushakov.tm.exception.general.InvalidIndexException;
import ru.aushakov.tm.exception.general.InvalidStatusException;
import ru.aushakov.tm.exception.general.NoUserLoggedInException;
import ru.aushakov.tm.model.AbstractBusinessEntity;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    protected IBusinessRepository<E> businessRepository;

    protected Class<E> currentClass;

    public AbstractBusinessService() {
    }

    public AbstractBusinessService(IBusinessRepository<E> repository, Class<E> currentClass) {
        this.repository = repository;
        this.businessRepository = repository;
        this.currentClass = currentClass;
    }

    private E getNewEntity() {
        try {
            return currentClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public E add(final String name, final String description) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        if (StringUtils.isEmpty(description)) throw new EmptyDescriptionException();
        final E entity = getNewEntity();
        entity.setName(name);
        entity.setDescription(description);
        businessRepository.add(entity);
        return entity;
    }

    @Override
    public E findOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.findOneByName(name);
    }

    @Override
    public E removeOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.removeOneByName(name);
    }

    @Override
    public E updateOneById(
            final String id,
            final String name,
            final String description
    ) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateOneById(id, name, description);
    }

    @Override
    public E updateOneByIndex(
            final Integer index,
            final String name,
            final String description
    ) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateOneByIndex(index, name, description);
    }

    @Override
    public E startOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.startOneById(id);
    }

    @Override
    public E startOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.startOneByIndex(index);
    }

    @Override
    public E startOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.startOneByName(name);
    }

    @Override
    public E finishOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.finishOneById(id);
    }

    @Override
    public E finishOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.finishOneByIndex(index);
    }

    @Override
    public E finishOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.finishOneByName(name);
    }

    @Override
    public E changeOneStatusById(String id, Status status) {
        return null;
    }

    @Override
    public E changeOneStatusByIndex(Integer index, Status status) {
        return null;
    }

    @Override
    public E changeOneStatusByName(String name, Status status) {
        return null;
    }

    @Override
    public E changeOneStatusById(final String id, final String statusId) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return businessRepository.changeOneStatusById(id, status);
    }

    @Override
    public E changeOneStatusByIndex(final Integer index, final String statusId) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return businessRepository.changeOneStatusByIndex(index, status);
    }

    @Override
    public E changeOneStatusByName(final String name, final String statusId) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return businessRepository.changeOneStatusByName(name, status);
    }

}
