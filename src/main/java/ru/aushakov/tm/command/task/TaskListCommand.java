package ru.aushakov.tm.command.task;

import ru.aushakov.tm.api.service.ITaskService;
import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.SortType;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_LIST;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        List<Task> tasks;
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + " OR PRESS ENTER:");
        final SortType sortType = SortType.toSortType(TerminalUtil.nextLine());
        final ITaskService taskService = serviceLocator.getTaskService();
        if (sortType == null) tasks = taskService.findAll();
        else tasks = taskService.findAll(sortType.getComparator());
        if (tasks == null || tasks.size() < 1) {
            System.out.println("[NOTHING FOUND]");
            return;
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
