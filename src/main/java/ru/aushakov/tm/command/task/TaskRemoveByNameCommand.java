package ru.aushakov.tm.command.task;

import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_REMOVE_BY_NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove task by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeOneByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

}
