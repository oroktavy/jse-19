package ru.aushakov.tm.command.user;

import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.util.TerminalUtil;

public class UserCreateCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return TerminalConst.USER_CREATE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Create a user";
    }

    @Override
    public void execute() {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL (OPTIONAL):");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().add(login, password, email);
    }

}
