package ru.aushakov.tm.command.system;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;

import java.util.Collection;

public class CommandListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_COMMANDS;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show application commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMAND LIST]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.getName();
            if (StringUtils.isEmpty(name)) continue;
            System.out.println(name);
        }
    }

}
