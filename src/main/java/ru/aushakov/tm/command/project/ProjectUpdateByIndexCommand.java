package ru.aushakov.tm.command.project;

import ru.aushakov.tm.command.AbstractProjectCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.model.Project;
import ru.aushakov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().updateOneByIndex(index, name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

}
