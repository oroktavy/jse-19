package ru.aushakov.tm.command;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.ServiceLocator;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    @Override
    public String toString() {
        final String name = getName();
        final String arg = getArgument();
        final String description = getDescription();
        String result = "";
        if (!StringUtils.isEmpty(name)) result += name;
        if (!StringUtils.isEmpty(arg)) result += ": [" + arg + "]";
        if (!StringUtils.isEmpty(description)) result += " - " + description;
        return result;
    }

}
