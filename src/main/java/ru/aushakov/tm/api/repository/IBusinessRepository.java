package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    E findOneByName(String name);

    E removeOneByName(String name);

    E updateOneById(String id, String name, String description);

    E updateOneByIndex(Integer index, String name, String description);

    E startOneById(String id);

    E startOneByIndex(Integer index);

    E startOneByName(String name);

    E finishOneById(String id);

    E finishOneByIndex(Integer index);

    E finishOneByName(String name);

    E changeOneStatusById(String id, Status status);

    E changeOneStatusByIndex(Integer index, Status status);

    E changeOneStatusByName(String name, Status status);

}
