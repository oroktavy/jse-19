package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    Task assignTaskToProject(String taskId, String projectId);

    Task unbindTaskFromProject(String taskId);

    List<Task> findAllTasksByProjectId(String projectId);

    List<Task> removeAllTasksByProjectId(String projectId);

}
