package ru.aushakov.tm.api.service;

import ru.aushakov.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    boolean isUserAuthenticated();

    String getUserId();

    String getUserLogin();

    User getUser();

    void changePassword(String oldPassword, String newPassword);

}
