package ru.aushakov.tm.api.entity;

import java.util.Date;

public interface IHasEndDate {

    Date getEndDate();

    void setEndDate(Date endDate);

}
