package ru.aushakov.tm.api.entity;

import ru.aushakov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
