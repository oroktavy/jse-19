package ru.aushakov.tm.model;

import ru.aushakov.tm.api.entity.IWBS;
import ru.aushakov.tm.enumerated.Status;

import java.util.Date;

public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    protected String name;

    protected String description;

    protected Status status = Status.PLANNED;

    protected Date startDate;

    protected Date endDate;

    public AbstractBusinessEntity() {
    }

    public AbstractBusinessEntity(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
        if (status == null) return;
        switch (status) {
            case IN_PROGRESS:
                setStartDate(new Date());
                break;
            case COMPLETED:
                setEndDate(new Date());
                break;
        }
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
