package ru.aushakov.tm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public abstract class AbstractEntity implements Serializable {

    public AbstractEntity() {
    }

    protected String id = UUID.randomUUID().toString();

    protected Date created = new Date();

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

}
